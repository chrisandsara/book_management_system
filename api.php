<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
use Slim\Slim;
$app = new Slim();
$app->get('/books/:book_id', 'getBooks');
$app->post('/books', 'addBook');
$app->put('/books/:book_id', 'updateBook');
$app->delete('/books/:book_id', 'deleteBook');
$app->run();

function getBooks($book_id){

  $sql = "SELECT * FROM books WHERE book_id=:book_id";
  try{
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("book_id", $book_id);
    $stmt->execute();
    $book = $stmt->fetchObject();
    $db=null;
    responseJson(json_encode($book),200);
  } catch(PDOException $e){
    responseJson( '{"error":{"text":'. $e->getMessage().'}}', 500);
  }
  }

function addBook(){
    $request = Slim::getInstance()->request();
    $book = json_decode($request->getBody());
  $sql = "insert into books (Title, Author, Year_Published, Publisher, Genre) VALUES (:Title, :Author, :Year_Published, :Publisher, :Genre)";
  try{
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("Title", $book->Title);
    $stmt->bindParam("Author", $book->Author);
    $stmt->bindParam("Year_Published", $book->Year_Published);
    $stmt->bindParam("Publisher", $book->Publisher);
    $stmt->bindParam("Genre", $book->Genre);
    $stmt->execute();
    $book->Book_Id = $db->lastInsertId();
    $db = null;
    responseJson(json_encode($book),201);
  } catch(PDOException $e){
    responseJson( '{"error":{"text":'. $e->getMessage().'}}', 500);
  }
  }

function updateBook($book_id){
    $request=Slim::getInstance()->request();
    $body = $request->getBody();
    $book = json_decode($body);
    $sql = "UPDATE books SET Title=:Title, Author=:Author, Year_Published=:Year_Published, Publisher=:Publisher, Genre=:Genre WHERE book_id=:book_id";
}

function deleteBook($book_id){
    $sql = "DELETE FROM books WHERE book_id=:book_id";
    $book=$stmt->fetchObject();
    $db = null;
    responseJson(json_encode($book),201);
  } catch(PDOException $e){
    responseJson( '{"error":{"text":'. $e->getMessage().'}}', 500);
  }
}
    

function getConnection(){
  $dbhost="localhost";
  $dbuser="B00644564";
  $dbpass="8Uxj3EnZ";
  $dbname="B00644564";
  $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  return $dbh;
}

function responseJson($responseBody, $statusCode){
    $app = Slim::getinstance();
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response->status($statusCode);
    $response->body($responseBody);
}

 ?>
