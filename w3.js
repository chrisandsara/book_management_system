$(document).ready(function(){
    
    $("#btnSave").click(function(){
        
        var book=new Book($("#Title").val(),$("#Author").val(),$("#Year_Published").val(),$("#Publisher").val(),$("#Genre").val());
    
        $.ajax({
        type: "POST",
        dataType: "json",
        url: "api.php/books",
        data:JSON.stringify(book),
        success: showResponse,
        error: showError
    });
});
    
    function Book(Title, Author, Year_Published, Publisher, Genre){
        this.Title=Title;
        this.Author=Author;
        this.Year_Published=Year_Published;
        this.Publisher=Publisher;
        this.Genre=Genre;
    }
        
    
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "api.php/books",
        success: showAllBooks,
        error: showError
    });


function showAllBooks(responseData){
	$.each(responseData.books,function(index,books){
		$("#book_list").append("<li type='square'>Book Id:"+books.Book_Id+",Title:"+books.Title+",Author:"+books.Author+"Year_Published:"+books.Year_Published+",Publisher:"+books.Publisher+",Genre:"+books.Genre);
		$("#book_list").append("</li>");
		});
		}
    
    function showResponse(responseData){
        console.log(responseData);
    }

function showError() {
    alert("Sorry, there is a problem!");
}

});